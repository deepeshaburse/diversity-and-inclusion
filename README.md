# Diversity and Inclusion

The GNOME Foundation strongly encourage and empowers diversity and inclusion among its staff and volunteers.

Our team welcomes and encourages participation by everyone. To us it doesn’t matter how you identify yourself or how others perceive you: we welcome you.

We are a non profit organization and being open and accessible to everyone is in our fundamentals. We strongly believe that Diversity and Inclusion are key points for a healthy and successful community.

### Goals and Initiatives
1. Organizing Diverse and Inclusive events such as D&I hackfests, Diversity [BoFs](https://wiki.gnome.org/BoFs) within and outside the community.

In order to help make GNOME events more inclusive and diverse, we've started a wiki page to collect ideas and checklists for event organizers. Check it out and help us continue to build this resource: [How to Host Inclusive Events](https://wiki.gnome.org/InclusionAndDiversity/InclusiveEvents).

Other resources:

[Workshops](https://wiki.gnome.org/InclusionAndDiversity/WorkshopResources). This is a wiki page with some links to organizations that can help you organize workshops on a variety of D&I topics for your community.

2. Spreading Awareness about Diversity and Inclusion in Free Software.

3. Supporting programs committed to building diversity and inclusion in Free and Open Source Software communities.

4. Help Newcomers and Women in tech with their Open Source Journey.

5. Outreach

### Resources

#### Websites
[Open Source Diversity](https://opensourcediversity.org/) - This website has a lot of resources, but is geared towards European audiences. There may be opportunities to help this organization grow to include more people. It includes things like a list of open source projects which support underrepresented groups, recurring programs for mentorship, funding or networking, and more.

[Speakerinnen](https://speakerinnen.org/) - Find women who can speak on a variety of subjects related to open source. This website is sponsored by the German government and most speakers are European women.

#### Articles
Here are some resources and studies for everyone who is curious and wants to read and know more why Diversity and Inclusion is important for a healthy community.

1. [Diversity Strengthens and Grows Your Community](https://blog.higherlogic.com/diversity-strengthens-and-grows-your-community)

2. [Why Diversity Matters](https://www.uvm.edu/~provost/davidrosowsky/Essay-Why%20Diversity%20Matters%202.pdf)

3. [Why Diversity and Inclusion Matter](https://www.catalyst.org/research/why-diversity-and-inclusion-matter/)

4. [Diversity and Inclusion in Free and Open Source Software](https://foss-backstage.de/session/diversity-and-inclusion-free-open-source-software)

### Goals and Initiatives

#### Inclusive and Diverse Events
In order to help make GNOME events more inclusive and diverse, we've started a wiki page to collect ideas and checklists for event organizers. Check it out and help us continue to build this resource: [How to Host Inclusive Events](https://wiki.gnome.org/InclusionAndDiversity/InclusiveEvents).

#### Other resources:

[Workshops](https://wiki.gnome.org/InclusionAndDiversity/WorkshopResources). This is a wiki page with some links to organizations that can help you organize workshops on a variety of D&I topics for your community.

Channels of communication
* [Matrix](https://matrix.to/#/#diversity-and-inclusion:gnome.org)

We are present in https://discourse.gnome.org feel free to introduce yourself in our D&I category.

#### Members
* Regina Nkemchor
* NuritziSanchez
* Kristi Progri
* Molly de Blanc
* StellaMaris Njage
* Gaurav Agrawal
* Manuel Quiñones

#### Meetings
Meetings happen every second Monday of the month. The next date is usually decided in the meeting and written in the meeting notes.

Location: https://meet.gnome.org/dee-9kq-sfm-nof

[Meeting Notes](https://hedgedoc.gnome.org/p1xuc-O4Q0iqsEUkvUccRA?view)

[Meeting Archive](https://wiki.gnome.org/InclusionAndDiversity/Meetings)
